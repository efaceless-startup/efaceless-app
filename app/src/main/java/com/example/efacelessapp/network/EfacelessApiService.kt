package com.example.efacelessapp.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

//Por este momento solo será en localhost.
//private const val BASE_URL = "http://localhost:8080/api/"
private const val BASE_URL = "http://efaceless.xyz/api/"

/**
 * Build the Moshi object that Retrofit will be using, making sure to add the Kotlin adapter for
 * full Kotlin compatibility.
 */
private val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()

/**
 * Use the Retrofit builder to build a retrofit object using a Moshi converter with our Moshi
 * object pointing to the desired URL
 */
private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

interface EfacelessApiService{
    @GET("emotion")
    fun getEmotions():
            Deferred<List<Emotion>>


    @GET("emotion/type")
    fun getEmotionTypes():
            Deferred<List<String>>


    @Headers( "Content-Type: application/json;charset=UTF-8")
    @POST("emotion")
    fun doEmotion(@Body request: RequestBody):
            Call<EmotionResponse>


}

/**
 * A public Api object that exposes the lazy-initialized Retrofit service
 */
object EfacelessApi {
    val retrofitService:EfacelessApiService by lazy { retrofit.create(EfacelessApiService::class.java)}
}