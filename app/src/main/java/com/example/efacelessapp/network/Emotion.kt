package com.example.efacelessapp.network

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Emotion (
    val id: String,
    val type: String,
    val desc: String,
    val origin: String
) : Parcelable
