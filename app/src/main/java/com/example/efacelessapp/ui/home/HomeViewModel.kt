package com.example.efacelessapp.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.efacelessapp.network.EfacelessApi
import com.example.efacelessapp.network.Emotion
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.Exception

class HomeViewModel : ViewModel(){

    // The internal MutableLiveData String that stores the most recent response status
    private val _status = MutableLiveData<String>()
    private val _emotions = MutableLiveData<List<Emotion>>()
    private val _emotionTypeList = MutableLiveData<List<String>>()

    // The external immutable LiveData for the status String
    val status: LiveData<String>
        get() = _status

    val emotions: LiveData<List<Emotion>>
        get() = _emotions

    val emotionTypeList: LiveData<List<String>>
        get() = _emotionTypeList

    private val _navigateToSelectedEmotion = MutableLiveData<Emotion>()

    val navigateToSelectedEmotion: LiveData<Emotion>
        get() = _navigateToSelectedEmotion

    // Create a Coroutine scope using a job to be able to cancel when needed
    private val viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    /**
     * Call getEmotions() on init so we can display status immediately.
     */
    init{
        getEmotions()
        getEmotionTypes()
    }

    /**
     * Sets the value of the status LiveData to the Efaceless API status.
     */
    private fun getEmotions() {
        coroutineScope.launch {
            // Get the Deferred object for our Retrofit request
            var emotionsDeferred = EfacelessApi.retrofitService.getEmotions();
            try {
                // Await the completion of our Retrofit request
                var listResult = emotionsDeferred.await()
                _status.value = "Success: ${listResult.size} Emotions retrieved"

                if (listResult.size > 0){
                    _emotions.value = listResult
                }
            } catch (e: Exception) {
                _status.value = "Failure: ${e.message}"
            }
        }
    }


    /**
     * Sets the value of the status LiveData to the Efaceless API status.
     */
    private fun getEmotionTypes() {
        coroutineScope.launch {
            // Get the Deferred object for our Retrofit request
            var emotionTypesDeferred = EfacelessApi.retrofitService.getEmotionTypes();
            try {
                // Await the completion of our Retrofit request
                var listResult = emotionTypesDeferred.await()

                if (listResult.size > 0){
                    _emotionTypeList.value = listResult
                }
            } catch (e: Exception) {

            }
        }
    }

    /**
     * When the property is clicked, set the [_navigateToSelectedProperty] [MutableLiveData]
     * @param marsProperty The [MarsProperty] that was clicked on.
     */
    fun displayEmotionDetails(emotion: Emotion) {
        _navigateToSelectedEmotion.value = emotion
    }

    /**
     * After the navigation has taken place, make sure navigateToSelectedProperty is set to null
     */
    fun displayEmotionDetailsComplete() {
        _navigateToSelectedEmotion.value = null
    }

    /**
     * When the [ViewModel] is finished, we cancel our coroutine [viewModelJob], which tells the
     * Retrofit service to stop.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}