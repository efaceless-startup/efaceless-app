package com.example.efacelessapp.ui.emotiondetail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.efacelessapp.network.Emotion

/**
 * Simple ViewModel factory that provides the MarsProperty and context to the ViewModel.
 */
class EmotionDetailViewModelFactory (
    private val emotion: Emotion,
    private val application: Application) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EmotionDetailViewModel::class.java)) {
            return EmotionDetailViewModel(emotion, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}



