package com.example.efacelessapp.ui.addemotion

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.efacelessapp.R
import com.example.efacelessapp.databinding.FragmentAddEmotionBinding
import com.example.efacelessapp.network.EmotionResponse
import com.example.efacelessapp.ui.login.LoginViewModel

/**
 * A simple [Fragment] subclass.
 */
class AddEmotionFragment : Fragment() {

    private val addEmotionViewModel: AddEmotionViewModel by activityViewModels()

    private lateinit var addEmotionButton: Button
    private lateinit var binding: FragmentAddEmotionBinding
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_emotion, container, false)

        navController = findNavController()

        addEmotionButton = binding.addEmotionButton
        addEmotionButton.setOnClickListener {
            addEmotion(binding)
        }

        return binding.root
    }

    private fun addEmotion(binding: FragmentAddEmotionBinding?) {
        var emotionResponse: EmotionResponse = addEmotionViewModel.addEmotion(binding)

        //Resolver bugs
        if (emotionResponse.status == 0 || emotionResponse.status == 200 || emotionResponse.status == 201){
            val text = "Add Emotion sucessfull!"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(this.context, text, duration)
            toast.setGravity(Gravity.TOP, 0, 256)
            toast.show()

            navController.navigate(R.id.navigation_home)
        }else {
            //No se inserto
            val text = "Try again!"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(this.context, text, duration)
            toast.setGravity(Gravity.TOP, 0, 256)
            toast.show()
        }


    }


}
