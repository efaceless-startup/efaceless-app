package com.example.efacelessapp.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.efacelessapp.databinding.GridViewItemBinding
import com.example.efacelessapp.network.Emotion

class EmotionGridAdapter (val onClickListener: OnClickListener) :
    ListAdapter<Emotion, EmotionGridAdapter.EmotionViewHolder>(DiffCallback) {

    class EmotionViewHolder (private var binding: GridViewItemBinding)
        : RecyclerView.ViewHolder(binding.root){
        fun bind(emotion: Emotion){
            binding.emotion = emotion
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Emotion>(){
        override fun areItemsTheSame(oldItem: Emotion, newItem: Emotion): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Emotion, newItem: Emotion): Boolean {
            return oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EmotionGridAdapter.EmotionViewHolder {
        return EmotionViewHolder(GridViewItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: EmotionViewHolder, position: Int) {
        val emotion = getItem(position)
        holder.itemView.setOnClickListener{
            onClickListener.onClick(emotion)
        }
        holder.bind(emotion)
    }

    class OnClickListener(val clickListener: (emotion: Emotion) -> Unit) {
        fun onClick(emotion:Emotion) = clickListener(emotion)
    }

}