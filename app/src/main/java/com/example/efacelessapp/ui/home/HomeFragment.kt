package com.example.efacelessapp.ui.home

import android.content.Intent.getIntent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getColorStateList
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.efacelessapp.R
import com.example.efacelessapp.databinding.FragmentHomeBinding
import com.example.efacelessapp.ui.login.LoginViewModel
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable


class HomeFragment : Fragment() {

    //LoginViewModel
    private val viewModelLogin: LoginViewModel by activityViewModels()

    private lateinit var homeViewModel: HomeViewModel

    private lateinit var navController: NavController

    private lateinit var binding: FragmentHomeBinding
    /**
     * Lazily initialize our [HomeViewModel].
     */
    private val viewModelHome: HomeViewModel by lazy {
        ViewModelProvider(this@HomeFragment).get(HomeViewModel::class.java)
    }

    //private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_home,container, false)

        var infoActivity: String = this.context.toString()

        navController = findNavController()
        //A modificar cuanto antes.
        if (infoActivity.indexOf("MainActivity")>-1){
            Log.i("INFO", this.context.toString())
            viewModelLogin.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
                when (authenticationState) {
                    LoginViewModel.AuthenticationState.AUTHENTICATED -> showWelcomeMessage()
                    LoginViewModel.AuthenticationState.UNAUTHENTICATED -> navController.navigate(R.id.loginFragment)
                }
            })
        }

        // Giving the binding access to the OverviewViewModel
        binding.viewModelHome = viewModelHome

        // Sets the adapter of the emotionGrid RecyclerView
        binding.emotionsTabGrid.adapter = EmotionGridAdapter(EmotionGridAdapter.OnClickListener{
            viewModelHome.displayEmotionDetails(it)
        })

        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment
        binding.setLifecycleOwner(this)

        viewModelHome.emotionTypeList.observe(viewLifecycleOwner, object: Observer<List<String>> {
            override fun onChanged(data: List<String>?) {
                data ?: return
                val chipGroup = binding.emotionTypeList
                val inflator = LayoutInflater.from(chipGroup.context)

                val children = data.map { emotionType ->
                    val chip = inflator.inflate(R.layout.emotion_type, chipGroup, false) as Chip
                    chip.text = emotionType
                    chip.tag = emotionType
                    when(emotionType){
                        "anger" -> chip.chipBackgroundColor = ColorStateList.valueOf(ContextCompat.getColor(chipGroup.context, R.color.colorAngerEmotion))
                        "disgust" -> chip.chipBackgroundColor = ColorStateList.valueOf(ContextCompat.getColor(chipGroup.context, R.color.colorDisgustEmotion))
                        "fear" -> chip.chipBackgroundColor = ColorStateList.valueOf(ContextCompat.getColor(chipGroup.context, R.color.colorFearEmotion))
                        "joy" -> chip.chipBackgroundColor = ColorStateList.valueOf(ContextCompat.getColor(chipGroup.context, R.color.colorJoyEmotion))
                        "sadness" -> chip.chipBackgroundColor = ColorStateList.valueOf(ContextCompat.getColor(chipGroup.context, R.color.colorSadnessEmotion))
                    }
//                    chip.setOnCheckedChangeListener { button, isChecked ->
//                        homeViewModel.onFilterChanged(button.tag as String, isChecked)
//                    }
                    chip
                }

                chipGroup.removeAllViews()

                for (chip in children) {
                    chipGroup.addView(chip)
                }
            }
        })

        // Observe the navigateToSelectedProperty LiveData and Navigate when it isn't null
        // After navigating, call displayPropertyDetailsComplete() so that the ViewModel is ready
        // for another navigation event.
        viewModelHome.navigateToSelectedEmotion.observe(this, Observer {
            if ( null != it ) {
                // Must find the NavController from the Fragment
                this.findNavController().navigate(HomeFragmentDirections.actionShowDetail(it))
                // Tell the ViewModel we've made the navigate call to prevent multiple navigation
                viewModelHome.displayEmotionDetailsComplete()
            }
        })

        return binding.root
    }

    private fun showWelcomeMessage() {

    }
}
