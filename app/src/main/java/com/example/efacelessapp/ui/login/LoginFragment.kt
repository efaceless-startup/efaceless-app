package com.example.efacelessapp.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.efacelessapp.FacelessTabActivity
import com.example.efacelessapp.R
import com.example.efacelessapp.databinding.FragmentLoginBinding

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by activityViewModels()

    private lateinit var usernameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var loginButton: Button

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentLoginBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_login, container, false)

        navController = findNavController()

        usernameEditText = binding.username
        passwordEditText = binding.password

        loginButton = binding.login
        loginButton.setOnClickListener {
            viewModel.authenticate(usernameEditText.text.toString(),
                passwordEditText.text.toString())
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            viewModel.refuseAuthentication()
            navController.popBackStack(R.id.titleFragment, false)
        }

        viewModel.authenticationState.observe(viewLifecycleOwner, Observer { authenticationState ->
            when (authenticationState) {
                LoginViewModel.AuthenticationState.AUTHENTICATED -> startTabActivity()
                LoginViewModel.AuthenticationState.INVALID_AUTHENTICATION -> showErrorMessage()
            }
        })

        return binding.root
    }

    private fun showErrorMessage() {
        val text = "Not user/password!"
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(this.context, text, duration)
        toast.setGravity(Gravity.TOP, 0, 256)
        toast.show()

        usernameEditText.getText().clear()
        passwordEditText.getText().clear()

        viewModel.refuseAuthentication()
        navController.popBackStack(R.id.loginFragment, false)
    }

    private fun startTabActivity(){
        val intent = Intent(this.context, FacelessTabActivity::class.java)
        intent.putExtra("session", true);
        startActivity(intent)
    }

}
