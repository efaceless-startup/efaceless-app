package com.example.efacelessapp.ui.emotiondetail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.efacelessapp.network.Emotion

class EmotionDetailViewModel(emotion: Emotion, app: Application) : AndroidViewModel(app) {

    private val _selectedEmotion = MutableLiveData<Emotion>()

    // The external LiveData for the SelectedProperty
    val selectedEmotion: LiveData<Emotion>
        get() = _selectedEmotion

    // Initialize the _selectedProperty MutableLiveData
    init {
        _selectedEmotion.value = emotion
    }

}