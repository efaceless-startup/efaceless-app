package com.example.efacelessapp.ui.emotiondetail

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.efacelessapp.R
import com.example.efacelessapp.databinding.FragmentEmotionDetailBinding
import kotlinx.coroutines.flow.callbackFlow

/**
 * A simple [Fragment] subclass.
 */
class EmotionDetailFragment : Fragment() {

    private lateinit var binding: FragmentEmotionDetailBinding
    private lateinit var navController: NavController

    private val emotionDetailViewModel: EmotionDetailViewModel by lazy {
        ViewModelProvider(this@EmotionDetailFragment).get(EmotionDetailViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val application = requireNotNull(activity).application
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_emotion_detail, container, false)
        binding.setLifecycleOwner(this)
        val emotion = EmotionDetailFragmentArgs.fromBundle(arguments!!).selectedEmotion
        val viewModelFactory = EmotionDetailViewModelFactory(emotion, application)

        binding.viewModel = ViewModelProviders.of(
            this, viewModelFactory).get(EmotionDetailViewModel::class.java)

        navController = findNavController()
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.fragment_emotion_detail)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.winner_menu, menu)
        // check if the activity resolves
        if(null == getShareIntent().resolveActivity(activity!!.packageManager)){
            // hide the menu item if it doesn't resolve
            menu?.findItem(R.id.share)?.setVisible(false)
        }
    }

    // Creating our Share Intent
    private fun getShareIntent(): Intent {
        //getString(R.string.share_success_text, args.numCorrect, args.numQuestions)
        val args = EmotionDetailFragmentArgs.fromBundle(arguments!!)
        val shareIntent= Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT, "Quiero compartir una emoción con vos!: "
                    + args.selectedEmotion.type + ", "
                    +args.selectedEmotion.desc + ".\n\r*EFECELESS : TUS EMOCIONES EN LIBERTAD*")
        return shareIntent
    }

    // Starting an Activity with our new Intent
    private fun shareSuccess() {
        startActivity(getShareIntent())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Log.i("INFO" , item!!.itemId.toString())
        when (item!!.itemId) {
            R.id.share -> shareSuccess()
            R.id.homeFragment -> navController.popBackStack()
        }

        //Parche
        if (item!!.title == null){
            navController.popBackStack()
        }
        return super.onOptionsItemSelected(item)
    }

}
