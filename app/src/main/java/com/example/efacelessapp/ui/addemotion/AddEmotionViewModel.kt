package com.example.efacelessapp.ui.addemotion

import android.annotation.SuppressLint
import android.util.Log
import android.widget.EditText
import android.widget.RadioButton
import androidx.lifecycle.ViewModel
import com.example.efacelessapp.databinding.FragmentAddEmotionBinding
import com.example.efacelessapp.network.EfacelessApi
import com.example.efacelessapp.network.Emotion
import com.example.efacelessapp.network.EmotionResponse
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddEmotionViewModel : ViewModel(){

    /*// Create a Coroutine scope using a job to be able to cancel when needed
    private val viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    init {

    }*/


    /**
     * Sets the value of the status LiveData to the Efaceless API status.
     */
    //fun addEmotion(binding: FragmentAddEmotionBinding?) {
    //fun addEmotion() {
        //coroutineScope.launch {
        //    var emotion: Emotion = Emotion("100","joy", "Hola Mundo!", "Mdz, Arg")
        //    EfacelessApi.retrofitService.doEmotion(emotion)
        //}
    //}

    fun addEmotion(binding: FragmentAddEmotionBinding?):EmotionResponse{
        val json = JSONObject()

        val disgustType: RadioButton = binding?.disgustRadioButton!!
        val fearType: RadioButton = binding?.fearRadioButton!!
        val joyType: RadioButton = binding?.joyRadioButton!!
        val sadnessType: RadioButton = binding?.sadnessRadioButton!!
        val angerType: RadioButton = binding?.angerRadioButton!!

        val desc : EditText = binding?.emotionText!!

        var type: String = "joy"

        if(disgustType.isChecked)type = "disgust"
        if(fearType.isChecked)type = "fear"
        if(joyType.isChecked)type = "joy"
        if(sadnessType.isChecked)type = "sadness"
        if(angerType.isChecked)type = "anger"

        json.put("type", type)
        json.put("desc", desc.text)
        json.put("origin", "Mdza, Arg")


        var responseEmotion = EmotionResponse()
        val requestBody: RequestBody = RequestBody.create(MediaType.parse("application/json"), json.toString())
        val call: Call<EmotionResponse> = EfacelessApi.retrofitService.doEmotion(requestBody)
        call.enqueue(object : Callback<EmotionResponse> {
            @SuppressLint("CommitPrefEdits")
            override fun onResponse(
                call: Call<EmotionResponse>,
                response: Response<EmotionResponse>
            ) {
                responseEmotion = EmotionResponse(
                    response.code(),
                    "",
                    "",
                    response.message()
                    )
                Log.i("INFO", "CODE RESPONSE: " + response.code() )
                Log.i("INFO", "MESSAGE RESPONSE: " + response.message())
            }

            override fun onFailure(call: Call<EmotionResponse>, t: Throwable) {
                Log.i("INFO", t.message)
                responseEmotion = EmotionResponse(
                    t.hashCode(),
                    "",
                    "",
                    t.message.toString()
                )
            }
        })
        return responseEmotion
    }

    /**
     * When the [ViewModel] is finished, we cancel our coroutine [viewModelJob], which tells the
     * Retrofit service to stop.
     */
    override fun onCleared() {
        super.onCleared()
        //viewModelJob.cancel()
    }
}