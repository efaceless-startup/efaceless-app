package com.example.efacelessapp

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import androidx.core.content.ContextCompat

/**
 * Returns a int representing the color emotion.
 */
fun convertTypeEmotionToColor(context: Context, type: String): Int {
    var colorEmotion : Int = Color.parseColor("#"+Integer.toHexString(ContextCompat.getColor(context, R.color.colorJoyEmotion)))

    when (type) {
        "disgust" -> colorEmotion = Color.parseColor("#"+Integer.toHexString(ContextCompat.getColor(context, R.color.colorDisgustEmotion)))
        "fear" -> colorEmotion = Color.parseColor("#"+Integer.toHexString(ContextCompat.getColor(context, R.color.colorFearEmotion)))
        "joy" -> colorEmotion = Color.parseColor("#"+Integer.toHexString(ContextCompat.getColor(context, R.color.colorJoyEmotion)))
        "sadness" -> colorEmotion = Color.parseColor("#"+Integer.toHexString(ContextCompat.getColor(context, R.color.colorSadnessEmotion)))
        "anger" -> colorEmotion = Color.parseColor("#"+Integer.toHexString(ContextCompat.getColor(context, R.color.colorAngerEmotion)))
    }

    return colorEmotion
}