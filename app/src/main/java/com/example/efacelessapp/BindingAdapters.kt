package com.example.efacelessapp

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.efacelessapp.network.Emotion
import com.example.efacelessapp.ui.home.EmotionGridAdapter
import com.google.android.material.chip.Chip

@BindingAdapter("listData")
fun recyclerViewData(recyclerView: RecyclerView, data: List<Emotion>?){
    val adapter = recyclerView.adapter as EmotionGridAdapter
    adapter.submitList(data)
}

@BindingAdapter("colorEmotion")
fun bindColor(textView: TextView, type: String?) {
    type?.let {
        var color: Int = convertTypeEmotionToColor(textView.context, type)
        textView.setBackgroundColor(color)
    }
}

@BindingAdapter("colorChipEmotion")
fun bindColorChip(chip: Chip, type: String?) {
    type?.let {
        var color: Int = convertTypeEmotionToColor(chip.context, type)

        chip.setBackgroundColor(color)
    }
}